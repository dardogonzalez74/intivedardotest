package com.intive.dardo.test.helpers

import android.content.res.Resources
import android.graphics.*
import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation

object ImageHelper {
    fun loadImage(url: String?,
                  imageView: ImageView,
                  @DrawableRes placeholder: Int? = null,
                  @DrawableRes error: Int? = null) {
        Picasso.get()
        val requestCreator = Picasso.get().load(url).fit()
        placeholder?.run { requestCreator.placeholder(placeholder) }
        error?.run { requestCreator.error(error) }
        requestCreator.into(imageView)
    }

    fun loadRoundedBorderImage(
            url: String?,
            imageView: ImageView,
            radiusInDp: Int,
            @DrawableRes placeholder: Int? = null,
            @DrawableRes error: Int? = null) {

        url?.run {
            val requestCreator = Picasso.get().load(url).transform(RoundedBorderTransform(radiusInDp)).fit()
            placeholder?.run { requestCreator.placeholder(placeholder) }
            error?.run { requestCreator.error(error) }
            requestCreator.into(imageView)
        } ?: run {
            placeholder?.run { Picasso.get().load(placeholder) }
        }
    }

    internal class RoundedBorderTransform(radiusInDp: Int) : Transformation {
        private val radiusInPx = dpToPx(radiusInDp)

        override fun transform(source: Bitmap): Bitmap {
            val size = Math.min(source.width, source.height)

            val x = (source.width - size) / 2
            val y = (source.height - size) / 2

            val squaredBitmap = Bitmap.createBitmap(source, x, y, size, size)
            if (squaredBitmap != source) {
                source.recycle()
            }

            val bitmap = Bitmap.createBitmap(size, size, source.config)
            val canvas = Canvas(bitmap)
            val paint = Paint()
            val shader = BitmapShader(squaredBitmap,
                    Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            paint.shader = shader
            paint.isAntiAlias = true

            val rect = Rect(0, 0, size, size)
            val rectF = RectF(rect)
            canvas.drawRoundRect(rectF, radiusInPx.toFloat(), radiusInPx.toFloat(), paint)

            squaredBitmap.recycle()
            return bitmap
        }

        override fun key(): String {
            return "circle"
        }
    }

    fun dpToPx(dp: Int): Int {
        return (dp * Resources.getSystem().displayMetrics.density).toInt()
    }

}