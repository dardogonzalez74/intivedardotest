package com.intive.dardo.test.apiservices.backendmodels

class UserBackend {
    lateinit var name: Name
    lateinit var email: String
    lateinit var picture: Picture

    class Name {
        lateinit var first: String
        lateinit var last: String
    }

    class Picture {
        lateinit var large: String
        lateinit var thumbnail: String
    }
}