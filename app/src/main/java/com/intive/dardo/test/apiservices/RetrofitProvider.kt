package com.intive.dardo.test.apiservices

import android.content.Context
import com.intive.dardo.test.apiservices.interceptors.LoggingInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitProvider<T>(val context: Context, private val service: Class<T>, private val serviceUrl: String) {

    fun provide() = provideRetrofit().create(service)!!

    private fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getOkHttpClient())
                .build()
    }

    private fun getOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        LoggingInterceptor.addInterceptor(builder)
        builder.readTimeout(30, TimeUnit.SECONDS)
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.writeTimeout(30, TimeUnit.SECONDS)
        return builder.build()
    }
}