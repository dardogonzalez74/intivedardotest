package com.intive.dardo.test.repositories.mappers

import com.intive.dardo.test.apiservices.backendmodels.UserBackend
import com.intive.dardo.test.apiservices.backendmodels.UserListBackend
import com.intive.dardo.test.entities.User

object UserMapper {
    fun map(userBackend: UserBackend): User {
        val user = User()
        user.firstname = userBackend.name.first
        user.lastname = userBackend.name.last
        user.email = userBackend.email
        user.thumbnail = userBackend.picture.thumbnail
        user.picture = userBackend.picture.large
        return user
    }

    fun map(userListBackend: UserListBackend): ArrayList<User> {
        val users = ArrayList<User>()
        userListBackend.results?.forEach { users.add(map(it)) }
        return users
    }
}