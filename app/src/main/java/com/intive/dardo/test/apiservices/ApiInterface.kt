package com.intive.dardo.test.apiservices

import com.intive.dardo.test.apiservices.backendmodels.UserListBackend
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("api")
    fun getUsers(@Query("page") page: Int,
                @Query("results") results: Int,
                @Query("seed") seed: String)
            : Call<UserListBackend>

}