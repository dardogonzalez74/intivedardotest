package com.intive.dardo.test.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class User: Parcelable {
        var firstname: String? = null
        var lastname: String? = null
        var email: String? = null
        var thumbnail: String? = null
        var picture: String? = null
}