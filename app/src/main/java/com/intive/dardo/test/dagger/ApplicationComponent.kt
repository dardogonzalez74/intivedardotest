package com.intive.dardo.test.dagger

import com.intive.dardo.test.userinterface.viewmodels.UsersViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    ApiServiceModule::class,
    RepositoriesModule::class] )
interface ApplicationComponent {
    fun inject(usersViewModel: UsersViewModel)
}
