package com.intive.dardo.test.apiservices.interceptors

import com.intive.dardo.test.helpers.LogHelper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

object LoggingInterceptor {

    fun addInterceptor(builder: OkHttpClient.Builder) {
        val httpLoggingInterceptor = HttpLoggingInterceptor { LogHelper.debug(this, it) }
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        builder.addInterceptor(httpLoggingInterceptor)
    }
}