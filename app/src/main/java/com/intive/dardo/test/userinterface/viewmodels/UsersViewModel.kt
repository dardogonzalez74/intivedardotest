package com.intive.dardo.test.userinterface.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.support.annotation.NonNull
import com.intive.dardo.test.TestApplication
import com.intive.dardo.test.repositories.UserRepository
import javax.inject.Inject

class UsersViewModel(@NonNull application: Application) : AndroidViewModel(application) {

    private val pageSize = 20

    @Inject
    lateinit var userRepository: UserRepository

    init {
        TestApplication.dagger.inject(this)
    }

    fun getUsers() = userRepository.getPagedData(pageSize)
}