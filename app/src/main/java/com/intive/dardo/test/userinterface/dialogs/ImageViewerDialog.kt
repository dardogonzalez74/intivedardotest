package com.intive.dardo.test.userinterface.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.intive.dardo.test.R
import com.intive.dardo.test.entities.User
import com.intive.dardo.test.helpers.ImageHelper
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_image_viewer.*

class ImageViewerDialog: DialogFragment() {

    private lateinit var user: User

    companion object {
        private const val USER_KEY = "userKey"

        fun newInstance(user: User): ImageViewerDialog {
            val dialog = ImageViewerDialog()
            val args = Bundle()
            args.putParcelable(USER_KEY, user)
            dialog.arguments = args
            return dialog
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        user = arguments!!.getParcelable(USER_KEY)
        val view = inflater.inflate(R.layout.dialog_image_viewer, container, false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return view
    }

    override fun onStart() {
        super.onStart()
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        closeImageView.setOnClickListener { dismiss() }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ImageHelper.loadRoundedBorderImage(user.picture, pictureImageView, 10, R.drawable.loading)
        nameTextView.text = "${user.firstname} ${user.lastname}"
        emailTextView.text = user.email
    }

}