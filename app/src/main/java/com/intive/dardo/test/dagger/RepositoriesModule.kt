package com.intive.dardo.test.dagger

import android.content.Context
import com.intive.dardo.test.apiservices.ApiService
import com.intive.dardo.test.repositories.UserRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoriesModule {
    @Provides
    @Singleton
    fun providesUserRepository(appContext: Context, apiService: ApiService): UserRepository {
        return UserRepository(appContext, apiService)
    }
}