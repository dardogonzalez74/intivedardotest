package com.intive.dardo.test.userinterface.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.intive.dardo.test.R
import com.intive.dardo.test.apiservices.Resource
import com.intive.dardo.test.entities.User
import com.intive.dardo.test.helpers.ImageHelper
import com.intive.dardo.test.helpers.LogHelper
import com.intive.dardo.test.userinterface.adapters.ErrorRetrievingDataAdapter
import com.intive.dardo.test.userinterface.adapters.UsersPagedListAdapter
import com.intive.dardo.test.userinterface.dialogs.ImageViewerDialog
import com.intive.dardo.test.userinterface.viewmodels.UsersViewModel
import kotlinx.android.synthetic.main.activity_main.*
import android.util.DisplayMetrics
import android.view.WindowManager

class MainActivity : AppCompatActivity() {

    private lateinit var usersViewModel: UsersViewModel
    private var usersPagedListAdapter: UsersPagedListAdapter? = null
    private var errorRetrievingDataAdapter = ErrorRetrievingDataAdapter()
    private lateinit var gridLayoutManager: GridLayoutManager
    private val linearLayoutManager = LinearLayoutManager(this)
    private val dialogShowUserTag = "dialogShowUserTag"
    private var mp: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupActionBar()
        setupView()
        setupViewModel()
    }

    override fun onStop() {
        super.onStop()
        mp?.run { releaseMediaPlayer()}
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(R.string.activity_users_action_bar_title)
    }

    private fun setupView() {
        val gridColumns = calculateColumns()
        gridLayoutManager = GridLayoutManager(this, gridColumns)
        swipeRefreshLayout.setOnRefreshListener { onSwipeRefresh() }
    }

    private fun setupViewModel() {
        usersViewModel = ViewModelProviders.of(this).get(UsersViewModel::class.java)
        onSwipeRefresh()
    }

    //TODO ver de hacer resize de las imagenes para que no queden margenes
    // vacios entre estas cuando la división no de un numero entero
    private fun calculateColumns(): Int {
        val userImageWidth = 90 // ImageView witdh in activity_main_thumbs_item
        val dm = DisplayMetrics()
        val windowManager = getSystemService(WINDOW_SERVICE) as WindowManager
        windowManager.defaultDisplay.getMetrics(dm)
        val screenWidthInDP = Math.round(dm.widthPixels / dm.density)
        return screenWidthInDP/userImageWidth
    }

    private fun onSwipeRefresh() {
        usersViewModel.getUsers().observe(this, usersObserver)
    }

    private val usersObserver = Observer<Resource<Any>> { resource ->
        when(resource?.status) {
            Resource.Companion.Status.LOADING -> processLoading(resource)
            Resource.Companion.Status.SUCCESS -> processSuccess(resource)
            Resource.Companion.Status.ERROR -> processError(resource)
        }
    }

    private fun processLoading(resource: Resource<Any>) {
        if (resource.data is PagedList<*>) {
            usersPagedListAdapter = UsersPagedListAdapter(this) { openDetails(it)}
            if(resource.data.isNotEmpty() || (resource.data.isEmpty() && recyclerView.adapter == null)) {
                recyclerView.layoutManager = gridLayoutManager
                recyclerView.adapter = usersPagedListAdapter
            }
            val items = resource.data as PagedList<User>
            usersPagedListAdapter?.submitList(items)
        }
        usersPagedListAdapter
                ?.isLoading(true)
                ?:run{swipeRefreshLayout.isRefreshing = true}
    }

    private fun processSuccess(resource: Resource<Any>) {
        swipeRefreshLayout.isRefreshing = false
        usersPagedListAdapter?.isLoading(false)
    }

    private fun processError(resource: Resource<Any>) {
        // TODO Administrar la forma que se muestran los errores al usuario
        // Por ahora se muestra un mensaje user friendly sin especificar la causa
        //      val error = resource.resourceException?.message?:getString(R.string.error_retrieving_users_from_server)!!
        val error = getString(R.string.error_retrieving_users_from_server)!!

        swipeRefreshLayout.isRefreshing = false
        if(recyclerView.adapter == errorRetrievingDataAdapter)
            return

        usersPagedListAdapter
                ?.run {
                    isLoading(false)
                    Toast.makeText(this@MainActivity, error, Toast.LENGTH_SHORT).show()
                }
                ?: run {
                    errorRetrievingDataAdapter.message = error
                    recyclerView.layoutManager = linearLayoutManager
                    recyclerView.adapter = errorRetrievingDataAdapter
                }
    }

    private fun openDetails(user: User) {
        ImageViewerDialog.newInstance(user).show(supportFragmentManager, dialogShowUserTag)
        mp = MediaPlayer.create(this, R.raw.alert)
        LogHelper.debug("mp=${mp.toString()}")
        mp?.setOnCompletionListener { releaseMediaPlayer() }
        mp?.start()
    }

    private fun releaseMediaPlayer() {
        mp?.release()
        mp = null
    }


}
