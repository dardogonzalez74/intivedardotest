package com.intive.dardo.test.repositories

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.paging.DataSource
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.content.Context
import android.support.annotation.MainThread
import com.intive.dardo.test.apiservices.ApiService
import com.intive.dardo.test.apiservices.Resource
import com.intive.dardo.test.entities.User
import com.intive.dardo.test.repositories.datasources.UserDataSource

class UserRepository(private val appContext: Context, private val apiService: ApiService) {

    @MainThread
    fun getPagedData(pageSize: Int): LiveData<Resource<Any>> {
        val resourcePagedListLiveData = MediatorLiveData<Resource<Any>>()
        val dataSourceFactory = DataSourceFactory(resourcePagedListLiveData)
        val pagedListConfig = PagedList.Config.Builder().setInitialLoadSizeHint(pageSize).setPageSize(pageSize).build()
        val pagedListLiveData = LivePagedListBuilder(dataSourceFactory, pagedListConfig).build()
        resourcePagedListLiveData.addSource(pagedListLiveData) { resourcePagedListLiveData.value = Resource.loading(it) }
        return resourcePagedListLiveData
    }

    inner class DataSourceFactory(private val resourcePagedListLiveData: MediatorLiveData<Resource<Any>>)
        : DataSource.Factory<Int, User>() {

        override fun create() = UserDataSource(appContext, apiService, resourcePagedListLiveData)
    }

}