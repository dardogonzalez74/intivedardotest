package com.intive.dardo.test.repositories.datasources

import android.arch.lifecycle.MediatorLiveData
import android.arch.paging.ItemKeyedDataSource
import android.content.Context
import com.intive.dardo.test.R
import com.intive.dardo.test.apiservices.ApiService
import com.intive.dardo.test.apiservices.Resource
import com.intive.dardo.test.apiservices.backendmodels.UserListBackend
import com.intive.dardo.test.entities.User
import com.intive.dardo.test.repositories.mappers.UserMapper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserDataSource(
        private val appContext: Context,
        private val apiService: ApiService,
        private val resourcePagedListLiveData: MediatorLiveData<Resource<Any>>)
    : ItemKeyedDataSource<Int, User>() {

    private var page = 0
    private val seed = "intive.dardo.test"

    override fun getKey(item: User) = page

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<User>) {
        val request = apiService.getUsers(page, params.requestedLoadSize, seed)
        resourcePagedListLiveData.postValue(Resource.loading())
        try {
            val response = request.execute()
            processResponse(response, callback)
        } catch (ex: Exception) {
            fireErrorResponse(ex.message)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<User>) {
        resourcePagedListLiveData.postValue(Resource.loading())
        apiService.getUsers(page, params.requestedLoadSize, seed).enqueue(UserPageCallback(callback))
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<User>) {
        //Do nothing
    }

    inner class UserPageCallback(private val callback: ItemKeyedDataSource.LoadCallback<User>)
        : Callback<UserListBackend> {

        override fun onFailure(call: Call<UserListBackend>, throwable: Throwable) {
            fireErrorResponse(throwable.message)
        }

        override fun onResponse(call: Call<UserListBackend>, response: Response<UserListBackend>) {
            processResponse(response, callback)
        }
    }

    private fun processResponse(response: Response<UserListBackend>, callback: ItemKeyedDataSource.LoadCallback<User>) {
        response.body()?.run {
            if (response.isSuccessful) {
                processSuccessfulResponse(response, callback)
                return
            }
        }
        fireErrorResponse(appContext.getString(R.string.error_retrieving_users_from_server))
    }

    private fun fireErrorResponse(message: String?) {
        val error = message?:appContext.getString(R.string.error_retrieving_users_from_server)
        resourcePagedListLiveData.postValue(Resource.error(error))
    }

    private fun processSuccessfulResponse(response: Response<UserListBackend>, callback: ItemKeyedDataSource.LoadCallback<User>) {
        page++
        val users = UserMapper.map(response.body()!!)
        callback.onResult(users)
        resourcePagedListLiveData.postValue(Resource.success(Any()))

    }
}