package com.intive.dardo.test

import android.app.Application
import com.intive.dardo.test.dagger.*

class TestApplication: Application() {
    companion object {
        private lateinit var instance: TestApplication
        lateinit var dagger: ApplicationComponent

        fun getInstance() = instance
        fun getContext() = instance.applicationContext!!
    }

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        dagger = DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .apiServiceModule(ApiServiceModule())
                .repositoriesModule(RepositoriesModule())
                .build()
    }}