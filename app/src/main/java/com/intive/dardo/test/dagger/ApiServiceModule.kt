package com.intive.dardo.test.dagger

import android.content.Context
import com.intive.dardo.test.apiservices.ApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApiServiceModule {
    @Provides
    @Singleton
    fun providesApiService(context: Context): ApiService {
        return ApiService(context)
    }
}