package com.intive.dardo.test.apiservices

import android.content.Context

class ApiService(val context: Context) {
    private var api: ApiInterface

    // TODO Levantar la url dependiendo del ambiente debug/prod desde build.gradle
    private val serviceUrl = "https://randomuser.me/"

    init {
        api = RetrofitProvider(context, ApiInterface::class.java, serviceUrl).provide()
    }

    fun getUsers(page: Int, results: Int, seed: String) = api.getUsers(page, results, seed)
}