package com.intive.dardo.test.userinterface.adapters

import android.arch.paging.PagedListAdapter
import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.intive.dardo.test.R
import com.intive.dardo.test.entities.User
import com.intive.dardo.test.helpers.ImageHelper
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main_loading_item.view.*

class UsersPagedListAdapter(private val context: Context, private val openDetails: (user: User) -> Unit) : PagedListAdapter<User, RecyclerView.ViewHolder>(POST_COMPARATOR) {

    private val layoutInflater = LayoutInflater.from(context)!!
    private var loading = false

    companion object {
        val POST_COMPARATOR = object : DiffUtil.ItemCallback<User>() {
            override fun areContentsTheSame(oldItem: User, newItem: User) =
                    oldItem == newItem

            override fun areItemsTheSame(oldItem: User, newItem: User) =
                    oldItem.firstname == newItem.firstname &&
                            oldItem.lastname == newItem.lastname &&
                            oldItem.email == newItem.email &&
                            oldItem.thumbnail == newItem.thumbnail &&
                            oldItem.picture == newItem.picture
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.activity_main_thumb_item -> bindViewHolderThumbItem(holder, position)
            R.layout.activity_main_loading_item -> (holder as ViewHolderLoadingItem).bind(loading)
        }
    }

    private fun bindViewHolderThumbItem(holder: RecyclerView.ViewHolder, position: Int) {
        val user = getItem(position)!!
        val viewHolderThumbItem = holder as ViewHolderThumbItem
        val imageView = viewHolderThumbItem.photoImageView
        ImageHelper.loadImage(user.thumbnail, imageView, R.drawable.loading)
        imageView.setOnClickListener(ViewHolderThumbItemClick(user))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (loading && position == itemCount - 1) return R.layout.activity_main_loading_item
        return R.layout.activity_main_thumb_item
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            when (viewType) {
                R.layout.activity_main_thumb_item ->
                    ViewHolderThumbItem(layoutInflater.inflate(R.layout.activity_main_thumb_item, parent, false))
                R.layout.activity_main_loading_item ->
                    ViewHolderLoadingItem(layoutInflater.inflate(R.layout.activity_main_loading_item, parent, false))
                else -> throw IllegalArgumentException("unknown view type $viewType")
            }

    fun isLoading(isLoading: Boolean) {
        loading = isLoading
        if (isLoading) notifyItemInserted(super.getItemCount())
        else notifyItemRemoved(super.getItemCount())
    }

    class ViewHolderThumbItem(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var photoImageView: ImageView = itemView.findViewById(R.id.thumbImageView)
    }

    class ViewHolderLoadingItem(view: View) : RecyclerView.ViewHolder(view) {
        private val progressBar = view.progressBar!!

        fun bind(isLoading: Boolean) {
            progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        }
    }

    inner class ViewHolderThumbItemClick(private val user: User): View.OnClickListener {
        override fun onClick(v: View?) {
            openDetails(user)
        }
    }
}
